package net.qrab.sleepytown;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.HashMap;
import java.util.Map;

class Actor extends java.awt.Rectangle {

  private static final long    serialVersionUID = 1L;

  private int                  col, row;

  private Actor.action         theAction;
  private Actor.type           theType;

  private String               name;

  Constants.ActorAttributes    aa;
  private Map<String, Integer> attrib           = new HashMap<String, Integer>();

  private Actor                target           = null;

  private Point                destination;
  private Color                borderColor      = new Color(0, 0, 0, 0);
  private Sprite               theSprite;
  private Projectile           theProjectile    = null;

  private final int            frameDelay       = 40000000;
  private long                 lastUpdate       = System.nanoTime();
  private final double         step             = 20;

  enum action {
    NOACTION, EXPLODE, ATTACK, HEAL, TREASURE
  }

  enum type {
    ACTOR, BLOCK, DEVICE, BACKGROUND
  }

  enum target {
    HPMIN, HPMAX, CLOSEST, RANDOM
  }

  enum status {
    IDLE, HEALING, ATTACKING, THINKING
  }

  // new random actor by object class (actor, device, block, bg)
  public Actor(Actor.type t) {
    this(Constants.getRandomActorName(t));
  }

  public Actor(String n) {
    name = n;
    setSize(Constants.tileSize, Constants.tileSize);
    getAttributes(name);
    theSprite = new Sprite(name);
    theSprite.addFrames(Constants.SpriteData.getByName(name).getFrames());
  }

  public Sprite getSprite() {
    return theSprite;
  }

  void getAttributes(String n) {
    aa = Constants.ActorAttributes.getByName(name);
    theType = Constants.ActorAttributes.getByName(n).getType();
    theAction = Constants.ActorAttributes.getByName(n).getAction();
    setAttrib("range", aa.range);
    setAttrib("hpmax", aa.hpmax);
    setAttrib("spmax", aa.spmax);
    setAttrib("apmax", aa.apmax);
    setAttrib("hp", aa.hpmax);
    setAttrib("sp", aa.spmax);
    setAttrib("ap", aa.apmax);
  }

  protected void setAttrib(String k, int v) {
    attrib.put(k, v);
  }

  protected int getAttrib(String k) {
    return attrib.get(k);
  }

  void setState(Actor.status s) {
    theSprite.setState(s);
  }

  Actor.status getState() {
    return theSprite.getState();
  }

  public String getName() {
    return name;
  }

  int getTeam() {
    return getAttrib("team");
  }

  void setTeam(int t) {
    setAttrib("team", t);
  }

  int getCol() {
    return col;
  }

  int getRow() {
    return row;
  }

  void setGrid(int c, int r) {
    col = c;
    row = r;
  }

  private int distanceTo(Actor target) {
    return (int) (this.getLocation().distance(target.getLocation()) / Constants.tileSize);
  }

  boolean inRange(Actor target) {
    return this.equals(target) ? false
        : distanceTo(target) <= getAttrib("range");
  }

  int attack(Actor target) {
    if (target == null)
      return 0;
    if (Constants.RANDOM.nextInt(10) > 1) {
      theProjectile = new Projectile(Constants.ActorAttributes.getByName(name)
          .getProjectileName(), this, target);
      Constants.projectileCount++;
      setHLColor(TileSheet.HLATTACKING);
      theSprite.setState(Actor.status.ATTACKING);
      target.setHLColor(TileSheet.BTARGET);
      target.decHP();
    } else {
      // setHLColor(TileSheet.HLCLEAR);
      System.err.println("attack failed");
    }
    return target.getAttrib("hp");
  }

  int heal(Actor target) {
    if (target == null)
      return 0;
    if (Constants.RANDOM.nextInt(100) > 15) {
      theProjectile = new Projectile(Constants.ActorAttributes.getByName(name)
          .getProjectileName(), this, target);
      Constants.projectileCount++;
      setHLColor(TileSheet.HLHEALING);
      theSprite.setState(Actor.status.HEALING);
      target.setHLColor(TileSheet.BHEALED);
      target.incHP();
    } else {
      // setHLColor(TileSheet.HLCLEAR);
      System.err.println("heal failed");
    }
    return target.getAttrib("hp");
  }

  Color getHLColor() {
    return this.borderColor;
  }

  void setHLColor(Color c) {
    this.borderColor = c;
  }

  BufferedImage getImg() {
    return theSprite.getImg();
  }

  Actor.type getType() {
    return theType;
  }

  void setType(Actor.type t) {
    theType = t;
  }

  Actor.action getAction() {
    return theAction;
  }

  void setAction(Actor.action a) {
    theAction = a;
  }

  void decHP() {
    int hp = getAttrib("hp");
    hp = --hp >= 0 ? hp : 0;
    setAttrib("hp", hp);
  }

  void incHP() {
    int hp = getAttrib("hp");
    int hpmax = getAttrib("hpmax");
    hp = ++hp <= hpmax ? hp : hpmax;
    setAttrib("hp", hp);
  }

  boolean needsHealing() {
    return getAttrib("hp") < getAttrib("hpmax");
  }

  void setTarget(Actor t) {
    target = t;
  }

  Actor getTarget() {
    return target;
  }

  void paint(Graphics2D g, ImageObserver io) {
    g.drawImage(this.getImg(), (int) this.getX(), (int) this.getY(), io);
    g.setColor(getHLColor());
    g.fillRect((int) getX() + 2, (int) getY() + 2, 4, 4);
    if (theProjectile != null) {
      g.drawImage(theProjectile.getImg(), (int) theProjectile.getX(),
          (int) theProjectile.getY(), io);
    }
  }

  // TODO: queue multiple destinations?
  void slideTo(int x, int y) {
    Constants.mapMotion = true;
    destination = new Point(x, y);
  }

  void update() {
    theSprite.update();
    if (theProjectile != null) {
      theProjectile.update();
      if (theProjectile.impacted()) {
        theProjectile = null;
        setState(Actor.status.IDLE);
        Constants.projectileCount--;
      }
    }
    if ((System.nanoTime() - lastUpdate) > frameDelay) {
      lastUpdate = System.nanoTime();
      updateLocation();
    }
  }

  void updateLocation() {
    if (destination != null) {
      if (this.getLocation().distance(destination.getLocation()) > step) {
        Constants.mapMotion = true;
        double deltaX = destination.getX() - this.getX();
        double deltaY = destination.getY() - this.getY();
        double qbar = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
        this.translate((int) ((deltaX * step) / qbar),
            (int) ((deltaY * step) / qbar));
      } else {
        this.setLocation(destination.getLocation());
        Constants.mapMotion = false;
        // impact = true;
      }
    }
  }

  Point getDestination() {
    return destination;
  }

}
