package net.qrab.sleepytown;

import java.awt.Color;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Hashtable;

import javax.imageio.ImageIO;

class TileSheet {
  static BufferedImage                     bi;

  static int                               sheetWd;
  static int                               sheetHt;
  static int                               sheetCols;
  static int                               sheetRows;

  public static Hashtable<String, Integer> tileImage   = new Hashtable<String, Integer>();

  static Color                             BGCOLOR     = new Color(40, 30, 50);
  static Color                             BTARGET     = new Color(128, 0, 0);
  static Color                             BACTIVE     = new Color(255, 255, 0);
  static Color                             BHEALED     = new Color(0, 255, 0);
  static Color                             HLCLEAR     = new Color(0, 0, 0, 0);
  static Color                             HLHEALING   = new Color(0, 64, 255);
  static Color                             HLATTACKING = new Color(128, 64, 0);
  static Color                             HLSELECTED  = Color.YELLOW;

  public TileSheet(String fname) throws IOException {
    bi = ImageIO.read(getClass().getResource(fname));
    sheetWd = (int) bi.getWidth();
    sheetHt = (int) bi.getHeight();
    sheetCols = (int) (sheetWd / Constants.tileSize);
    sheetRows = (int) (sheetHt / Constants.tileSize);
  }

  public static void addTile(String n, Integer t) {
    tileImage.put(n, t);
  }

  public int getTile(String n) {
    return tileImage.get(n);
  }

  public static String[] getTileNames() {
    return tileImage.keySet().toArray(new String[0]);
  }

  public int getSize() {
    return Constants.tileSize;
  }

  static BufferedImage getTileImg(int n) {
    return getTileImg((n % sheetCols) * Constants.tileSize, (n / sheetCols) * Constants.tileSize);
  }

  static BufferedImage getTileImg(int tx, int ty) {
    return (bi.getSubimage(tx, ty, Constants.tileSize, Constants.tileSize));
  }

}
