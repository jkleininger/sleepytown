package net.qrab.sleepytown;

import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

class Queue extends java.util.concurrent.CopyOnWriteArrayList<Actor> {

  private static final long serialVersionUID = 1L;

  private int               queueCap;
  private int               team;
  private Rectangle         r;

  protected Queue(int team, int cap) {
    queueCap = cap;
    this.team = team;
  }

  void setBounds(int x, int y, int wd, int ht) {
    r = new Rectangle(x, y, wd, ht);
  }

  void fill() {
    while (!full())
      addActor(new Mob(Actor.type.ACTOR));
  }

  void addActor(Actor a) {
    a.setTeam(team);
    this.add(a);
  }

  boolean full() {
    return (this.size() >= queueCap);
  }

  void update() {
    if (!this.isEmpty())
      this.get(0).update();
  }

  int getPXWd() {
    return (Constants.tileSize);
  }

  int getPXHt() {
    return (Constants.tileSize);
  }

  int getX() {
    return (int) r.getX();
  }

  int getY() {
    return (int) r.getY();
  }

  Point getLocation() {
    return r.getLocation();
  }

  double getWidth() {
    return r.getWidth();
  }

  double getHeight() {
    return r.getHeight();
  }

  BufferedImage getImg() {
    return this.isEmpty() ? null : this.get(0).getImg();
  }

  Actor dequeue() {
    return this.remove(0);
  }

}
