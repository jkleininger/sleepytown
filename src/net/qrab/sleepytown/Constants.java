package net.qrab.sleepytown;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Random;

public class Constants {

  public static List<String> actorNames      = new ArrayList<String>();
  public static Random       RANDOM          = new Random();

  public static boolean      mapMotion       = false;
  public static int          projectileCount = 0;

  public static int          mapCols         = 8;
  public static int          mapRows         = 8;
  public static int          tileSize        = 32;

  public static int          numQueues       = 2;
  public static int          queueMax        = 3;

  public static String getRandomActorName() {
    return actorNames.get(RANDOM.nextInt(actorNames.size()));
  }

  public static String getRandomActorName(Actor.type t) {
    Actor.type candidateType;
    String name;
    do {
      name = getRandomActorName();
      candidateType = Constants.ActorAttributes.getByName(name).getType();
    } while (!candidateType.equals(t));
    return name;
  }

  public static enum SpriteData {
    myconid(new int[][] { { 0, 1, 2, 3 }, { 4 }, { 3, 4 }, { 4, 5 } }), aquaman(
        new int[][] { { 0, 1, 2 }, { 3, 4 }, { 5, 6 }, { 7, 8 } }), firedude(
        new int[][] { { 0, 1 }, { 2, 3, 4 }, { 5 }, { 6, 7 } }),

    firewisp(new int[][] { { 0, 1 } }), bubble(new int[][] { { 0, 1 } }), fireball(
        new int[][] { { 0, 1, 2, 3 } });

    private int[][] frameData;

    SpriteData(int[][] frames) {
      frameData = frames;
    }

    public static SpriteData getByName(String name) {
      for (SpriteData sd : values()) {
        if (sd.toString().equals(name))
          return sd;
      }
      return null;
    }

    public int[][] getFrames() {
      return frameData;
    }

  } // SpriteData

  public static enum ActorAttributes {

    myconid(0, "bubble", 5, 3, 3, 1, Actor.type.ACTOR, Actor.action.HEAL), aquaman(
        0, "fireball", 5, 3, 3, 1, Actor.type.ACTOR, Actor.action.ATTACK), firedude(
        0, "firewisp", 6, 3, 3, 1, Actor.type.ACTOR, Actor.action.ATTACK), Background(
        0, "", 0, 0, 0, 0, Actor.type.BACKGROUND, Actor.action.NOACTION);

    private Map<String, Integer> attrib = new HashMap<String, Integer>();

    private String               projectileName;

    private Actor.type           aType;
    private Actor.action         aAction;
    // private Actor.target aTarget;
    // private Actor.status aStatus;

    public int                   range, hpmax, spmax, apmax;

    ActorAttributes(int defaultAction, String projName, int range, int hpmax,
        int spmax, int apmax, Actor.type t, Actor.action a) {
      aType = t;
      aAction = a;
      this.range = range;
      this.hpmax = hpmax;
      this.spmax = spmax;
      this.apmax = apmax;
      setAttrib("range", range);
      setAttrib("hpmax", hpmax);
      setAttrib("spmax", hpmax);
      setAttrib("apmax", apmax);

      projectileName = projName;
      actorNames.add(this.toString());
    }

    public static ActorAttributes getByName(String name) {
      for (ActorAttributes aa : values()) {
        if (aa.toString().equals(name)) {
          return aa;
        }
      }
      return null;
    }

    public Actor.type getType() {
      return aType;
    }

    public Actor.action getAction() {
      return aAction;
    }

    public String getProjectileName() {
      return projectileName;
    }

    public static String getRandomActorName() {
      return actorNames.get(RANDOM.nextInt(actorNames.size()));
    }

    protected void setAttrib(String k, int v) {
      attrib.put(k, v);
    }

    protected int getAttrib(String k) {
      return attrib.get(k);
    }

  } // ActorAttributes

}
