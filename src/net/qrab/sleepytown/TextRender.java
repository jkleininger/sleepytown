package net.qrab.sleepytown;

import java.awt.Point;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.Hashtable;

import javax.imageio.ImageIO;

class TextRender {

  private BufferedImage                      bi;

  private int                                sheetWd, sheetHt;
  private int                                sheetCols, sheetRows;
  private int                                charWd, charHt;

  private static Hashtable<Character, Point> charmap = new Hashtable<Character, Point>();

  public TextRender(String fname) throws IOException {
    bi = ImageIO.read(getClass().getResource("fonts/" + fname + ".png"));
    sheetWd = (int) bi.getWidth();
    sheetHt = (int) bi.getHeight();
    sheetCols = (int) (sheetWd / Constants.tileSize);
    sheetRows = (int) (sheetHt / Constants.tileSize);
    charWd = 10;
    charHt = 10;
  }

  private BufferedImage getCharImg(Character c) {
    return bi.getSubimage((int) charmap.get(c).getX(), (int) charmap.get(c).getY(), charWd, charHt);
  }

  public void getStringImg(String theString) {
    int charCount = theString.length();
    BufferedImage retimg = new BufferedImage(charWd * charCount, charHt, BufferedImage.TYPE_INT_ARGB);
    for (char c : theString.toCharArray()) {
      System.err.print(c);
    }
    System.err.println("\n(" + retimg.getWidth() + "x" + retimg.getHeight() + ")");
  }

}
