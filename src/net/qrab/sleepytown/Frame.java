package net.qrab.sleepytown;

import java.awt.image.BufferedImage;

public class Frame {

  BufferedImage theImage;
  int           theDuration;

  public Frame(BufferedImage image, int duration) {
    theImage = image;
    theDuration = duration;
  }

  public BufferedImage getImage() {
    return theImage;
  }

  public void setImage(BufferedImage img) {
    theImage = img;
  }

  public int getDuration() {
    return theDuration;
  }

  public void setDuration(int duration) {
    theDuration = duration;
  }

}
