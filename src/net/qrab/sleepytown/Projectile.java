package net.qrab.sleepytown;

import java.awt.image.BufferedImage;

class Projectile extends java.awt.Rectangle {

  private static final long serialVersionUID = 1L;

  private Sprite            theSprite;

  private Actor             target;

  private int               unitsPerStep     = 5;
  private int               frameDelay       = 40000000;         // nanoseconds
  private long              lastUpdate       = System.nanoTime();

  private boolean           impact           = false;

  public Projectile(String name, Actor src, Actor dst) {
    target = dst;
    setLocation(src.getLocation());
    theSprite = new Sprite(name);
    theSprite.addFrames(Constants.SpriteData.getByName(name).getFrames());
  }

  BufferedImage getImg() {
    return theSprite.getImg();
  }

  void update() {
    theSprite.update();
    if ((System.nanoTime() - lastUpdate) > frameDelay) {
      lastUpdate = System.nanoTime();
      updateLocation();
    }
  }

  // TODO expire on impact
  void updateLocation() {
    if (this.getLocation().distance(target.getLocation()) > unitsPerStep) {
      double deltaX = target.getX() - this.getX();
      double deltaY = target.getY() - this.getY();
      double qbar = Math.sqrt((deltaX * deltaX) + (deltaY * deltaY));
      this.translate((int) ((deltaX * unitsPerStep) / qbar), (int) ((deltaY * unitsPerStep) / qbar));
    } else {
      this.setLocation(target.getLocation());
      impact = true;
    }
  }

  boolean impacted() {
    return impact;
  }

  @Override
  public String toString() {
    return "Projectile" + "\n" + "----------" + "\n" + "name:  " + theSprite.getName() + "\n" + "dst:   " + target.getLocation() + "\n" + "loc:   " + this.getLocation() + "\n" + "speed: "
        + unitsPerStep + "\n";
  }

}
