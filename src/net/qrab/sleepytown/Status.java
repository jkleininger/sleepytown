package net.qrab.sleepytown;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.image.BufferedImage;

class Status extends java.util.concurrent.CopyOnWriteArrayList<String> {

  private static final long serialVersionUID = 1L;

  private int               pxX, pxY, pxW, pxH;

  private int               lineH            = 12;

  private int               maxLines         = 2;

  private BufferedImage     bi;
  private Graphics2D        g;

  Status(int x, int y, int w, int h) {
    pxX = x;
    pxY = y;
    pxW = w;
    pxH = h;
    bi = new BufferedImage(pxW, pxH, BufferedImage.TYPE_INT_ARGB);
    g = bi.createGraphics();
  }

  int getX() {
    return this.pxX;
  }

  int getY() {
    return this.pxY;
  }

  int getWidth() {
    return this.pxW;
  }

  int getHeight() {
    return this.pxH;
  }

  void update() {
    if (this.size() > maxLines) {
      this.removeRange(0, this.size() - maxLines - 1);
    }
  }

  // concurrent arraylist doesn't include a removeRange method
  void removeRange(int n1, int n2) {
    for (; n1 < n2; n1++) {
      this.remove(n1);
    }
  }

  BufferedImage getImg() {
    g.setColor(new Color(90, 90, 90));
    g.fillRect(0, 0, pxW, pxH);
    g.setColor(new Color(192, 192, 90));
    int n = 0;

    for (String s : this) {
      g.drawString(s, 4, n++ * lineH + 4);
      g.drawString(s, 4, 4);
    }

    return bi;
  }

}
