package net.qrab.sleepytown;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;

import javax.swing.JFrame;
import javax.swing.JPanel;

public class sleepytown extends JPanel implements MouseListener {

  private static final long serialVersionUID = 1L;
  Level                     theLevel;
  static JFrame             theFrame         = new JFrame("sleepytown");

  int                       prefWd           = 500;
  int                       prefHt           = 500;

  boolean                   running          = true;

  public sleepytown() throws IOException {
    super();
    this.setFocusable(true);
    this.setPreferredSize(new Dimension(prefWd, prefHt));
    theFrame.addMouseListener(this);
    addMouseListener(this);

    theLevel = new Level();

    Thread renderThread = new Thread() {
      public void run() {
        renderLoop();
      }
    };
    renderThread.start();
  }

  private void renderLoop() {
    while (running) {
      theLevel.update();
      repaint();

      try {
        Thread.sleep(0);
      } catch (InterruptedException ex) {
      }
    }
  }

  /*
   * private void logicThread() { while (running) { theLevel.getGameMap().
   */

  protected void paintComponent(Graphics g) {
    g.drawImage(theLevel.getLevelImg(this), 0, 0, this);
  }

  public static void main(String[] args) throws IOException {
    Constants.ActorAttributes.values();
    theFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    theFrame.setContentPane(new sleepytown());
    theFrame.pack();
    theFrame.setVisible(true);
  }

  public void mousePressed(MouseEvent e) {
  }

  public void mouseReleased(MouseEvent e) {
  }

  public void mouseEntered(MouseEvent e) {
  }

  public void mouseExited(MouseEvent e) {
  }

  public void mouseClicked(MouseEvent e) {
    theLevel.processClick(e.getPoint());
    repaint();
  }

}
