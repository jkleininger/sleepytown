package net.qrab.sleepytown;

import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.util.List;

class GameMap {

  private int[][]       bg;

  int                   mapX, mapY, mapRows, mapCols, mapPXWd, mapPXHt;

  private BufferedImage bi;
  private Graphics2D    g;

  private int           turn      = 0;
  private int           teams     = 2;

  private List<Actor>   actorList = new java.util.concurrent.CopyOnWriteArrayList<Actor>();

  private boolean       active    = false;

  public GameMap() {

    Thread actionThread = new Thread() {
      public void run() {
        actionLoop();
      }
    };
    actionThread.start();

  }

  public void activate() {
    active = true;
  }

  // iterate through actors on board, giving each a chance to perform an action
  private void actionLoop() {
    while (true) {
      if (active) {
        for (Actor a : actorList) {
          if (a.getAction() != Actor.action.NOACTION) {
            while (Constants.projectileCount > 0 || Constants.mapMotion) {
              threadSleep(1);
            }
            doAction(a);
          } else {
            threadSleep(20);
          }
        }
        active = false;
      } else {
        threadSleep(500);
      }
    }
  }

  private void threadSleep(int ms) {
    try {
      Thread.sleep(ms);
    } catch (InterruptedException ex) {
    }
  }

  void init() {
    mapPXWd = mapCols * Constants.tileSize;
    mapPXHt = mapRows * Constants.tileSize;
    bg = new int[mapCols][mapRows];
    bi = new BufferedImage(Constants.tileSize * mapCols, Constants.tileSize * mapRows, BufferedImage.TYPE_INT_ARGB);
    g = bi.createGraphics();
  }

  void setBounds(int x, int y, int c, int r) {
    mapX = x;
    mapY = y;
    mapPXWd = Constants.tileSize * c;
    mapPXHt = Constants.tileSize * r;
    mapCols = c;
    mapRows = r;
    init();
  }

  Rectangle getBounds() {
    return new Rectangle(mapX, mapY, mapPXWd, mapPXHt);
  }

  int getX() {
    return mapX;
  }

  int getY() {
    return mapY;
  }

  int getWidth() {
    return mapPXWd;
  }

  int getHeight() {
    return mapPXHt;
  }

  int getCols() {
    return mapCols;
  }

  int getRows() {
    return mapRows;
  }

  int getBottom() {
    return mapY + mapPXHt;
  }

  int getRight() {
    return mapX + mapPXWd;
  }

  int getTeamCount() {
    return teams;
  }

  void setTeamCount(int n) {
    teams = n;
  }

  boolean motion() {
    for (Actor a : actorList)
      if (a.getDestination() != null)
        return true;
    return false;
  }

  void fill(int t) {
    for (int y = 0; y < mapRows; y++) {
      for (int x = 0; x < mapCols; x++) {
        bg[x][y] = t;
      }
    }
  }

  /*
   * void randomActors(int num, int actorType ) { for(int n=0 ; n<num ; n++) {
   * Point p = findOpen(); Actor a = new Actor(actorType);
   * System.err.println("added random: " + a); a.setTeam(-1); this.add( a,
   * (int)p.getX(), (int)p.getY() ); } }
   */

  Point findOpen() {
    int x, y;
    do {
      x = Constants.RANDOM.nextInt(this.getCols());
      y = Constants.RANDOM.nextInt(this.getRows());
    } while (this.occupant(x, y) != null);
    return new Point(x, y);
  }

  void update() {
    for (Actor a : actorList) {
      a.update();
      if (a.getType() != Actor.type.BLOCK && a.getAttrib("hp") <= 0)
        remove(a);
    }
  }

  void remove(Actor theActor) {
    actorList.remove(theActor);
  }

  void add(Actor newActor, Point colrow, Point slideFrom) {
    newActor.setGrid((int) colrow.getX(), (int) colrow.getY());
    newActor.setLocation((int) slideFrom.getX(), (int) slideFrom.getY());
    newActor.slideTo((int) colrow.getX() * Constants.tileSize, (int) colrow.getY() * Constants.tileSize);
    actorList.add(newActor);
  }

  Actor occupant(Point p) {
    return occupant((int) p.getX(), (int) p.getY());
  }

  Actor occupant(int c, int r) {
    for (Actor a : actorList) {
      if (a.getCol() == c && a.getRow() == r) {
        return a;
      }
    }
    return null;
  }

  /*
   * step major = delta major / delta major = 1 step minor = delta minor / delta
   * major < 1
   */
  boolean los(Actor a, Actor b) {
    if (a == null || b == null)
      return false;
    double dX = b.getX() - a.getX();
    double dY = b.getY() - a.getY();
    double f = Math.abs(dX) > Math.abs(dY) ? Math.abs(dX) : Math.abs(dY);
    dX = (dX / f) * Constants.tileSize;
    dY = (dY / f) * Constants.tileSize;
    // double x=(int)a.getX();
    // double y=(int)a.getY();
    int steps = (int) Math.abs(f / Constants.tileSize);
    for (int s = 0; s < Math.abs(steps); s++) {
      int c = (int) ((a.getX() + (dX * s)) / Constants.tileSize);
      int r = (int) ((a.getY() + (dY * s)) / Constants.tileSize);
      if (c >= 0 && c < mapCols && r >= 0 && r < mapRows) {
        Actor theOccupant = occupant(c, r);
        if (theOccupant != null && !theOccupant.equals(a))
          return false;
        else if (theOccupant != null && theOccupant.equals(b))
          return true;
      }
    }
    return true;
  }

  private void doAction(Actor theActor) {
    if (theActor.getAction() == Actor.action.ATTACK) {
      theActor.attack(findAttackTarget(theActor));
    } else if (theActor.getAction() == Actor.action.HEAL) {
      theActor.heal(findHealTarget(theActor));
    }
  }

  Actor findAttackTarget(Actor theAttacker) {
    int minHP = 1000000;
    Actor target = null;
    for (Actor candidate : actorList) {
      if (!candidate.equals(theAttacker) && los(theAttacker, candidate) && candidate.getTeam() != theAttacker.getAttrib("team") && candidate.getTeam() >= 0 && theAttacker.inRange(candidate)
          && candidate.getAttrib("hp") < minHP) {
        target = candidate;
        minHP = target.getAttrib("hp");
      }
    }

    System.err.println("found attack target: " + target);
    return target;
  }

  Actor findHealTarget(Actor theHealer) {
    int minHP = 1000000;
    Actor target = null;
    for (Actor candidate : actorList) {
      if (!candidate.equals(theHealer) && candidate.needsHealing() && los(theHealer, candidate) && candidate.getTeam() == theHealer.getTeam() && theHealer.inRange(candidate)
          && candidate.getAttrib("hp") < minHP) {
        target = candidate;
        minHP = target.getAttrib("hp");
      }
    }
    System.err.println("found heal target: " + target);
    return target;
  }

  void incTurn() {
    turn = ++turn > teams ? 0 : turn;
  }

  int getTurn() {
    return turn;
  }

  int getBG(int x, int y) {
    return bg[x][y];
  }

  void setBG(int x, int y, int n) {
    bg[x][y] = n;
  }

  BufferedImage getImg(TileSheet ts, ImageObserver io) {
    g.setColor(TileSheet.BGCOLOR);
    g.fillRect(0, 0, mapPXWd, mapPXHt);
    for (int y = 0; y < mapRows; y++) {
      for (int x = 0; x < mapCols; x++) {
        g.drawImage(ts.getTileImg(bg[x][y]), x * Constants.tileSize, y * Constants.tileSize, io);
      }
    }
    for (Actor a : actorList) {
      a.paint(g, io);
    }
    return bi;
  }

  @Override
  public String toString() {
    String retval = new String();
    for (int c = 0; c < mapCols; c++)
      retval += "+---";
    retval += "+\n";
    for (int r = 0; r < mapRows; r++) {
      for (int c = 0; c < mapCols; c++) {
        retval += occupant(c, r) != null ? "| x " : "|   ";
      }
      retval += "|\n";
      for (int c = 0; c < mapCols; c++)
        retval += "+---";
      retval += "+\n";
    }
    return retval;
  }
}
