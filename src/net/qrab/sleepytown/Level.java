package net.qrab.sleepytown;

import java.awt.Color;
import java.awt.Graphics2D;
import java.awt.Point;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;
import java.awt.image.ImageObserver;
import java.io.IOException;

class Level {

  int           initActors    = 4;                             // initial
                                                                // actors placed
  int           drawMargin    = 4;
  int           queueMax      = 15;                            // maximum
                                                                // actors that
                                                                // can be queued
                                                                // per level

  Rectangle     gameArea;
  BufferedImage gameBI;
  Graphics2D    gameG;

  TileSheet     theTileSheet;
  GameMap       theMap;
  Status        theStatus;

  Queue[]       theQueue      = new Queue[Constants.numQueues];

  Actor         selectedActor = null;

  public Level() throws IOException {

    theTileSheet = new TileSheet("assets/tileset.png");
    // theTileSheet = new TileSheet("assets/tiles.png", Constants.tileSize);

    TileSheet.addTile("Background", 0);

    theStatus = new Status(drawMargin, drawMargin, Constants.mapCols * Constants.tileSize, Constants.tileSize * 2);

    theMap = new GameMap();
    theMap.setBounds(drawMargin, theStatus.getY() + theStatus.getHeight() + drawMargin, Constants.mapCols, Constants.mapRows);
    theMap.setTeamCount(Constants.numQueues);

    gameArea = new Rectangle(0, 0, 600, 600);

    // gameArea = new Rectangle ( Constants.tileSize*mapCols+(2*drawMargin),
    // (int)(theStatus.getHeight()+theMap.getHeight()+playerQueue.getHeight()+(4*drawMargin))
    // );

    gameBI = new BufferedImage((int) gameArea.getWidth(), (int) gameArea.getHeight(), BufferedImage.TYPE_INT_ARGB);
    gameG = gameBI.createGraphics();

    for (int n = 0; n < Constants.numQueues; n++) {
      theQueue[n] = new Queue(n, queueMax);
      theQueue[n].fill();

      int myX = theMap.getWidth() / (Constants.numQueues + 1);
      myX *= n + 1;
      myX += drawMargin;
      myX -= (Constants.tileSize / 2);

      theQueue[n].setBounds(myX, theMap.getBottom() + drawMargin, Constants.tileSize, Constants.tileSize);
    }

    theMap.fill(theTileSheet.getTile("Background"));
    // theMap.randomActors(initActors,Actor.BLOCK);

    TextRender tr = new TextRender("thefont");
    tr.getStringImg("this is a junk");

  }

  public BufferedImage getLevelImg(ImageObserver io) {
    paintGameImg(io);
    return gameBI;
  }

  void update() {
    theMap.update();
    theStatus.update();
    for (Queue q : theQueue)
      q.update();
  }

  void doClick(Point p, Queue q) {
    if (!(Constants.mapMotion || Constants.projectileCount > 0)) {
      System.err.println("processing click");

      if (selectedActor == null) {
        selectedActor = theMap.occupant(p);
        if (selectedActor == null) {
          doAdd(p, q);
          theMap.incTurn();
        } else {
          selectedActor.setHLColor(TileSheet.HLSELECTED);
        }
      }

      else {

        if (theMap.occupant(p) == null) {
          selectedActor.setGrid((int) p.getX(), (int) p.getY());
          selectedActor.slideTo((int) (p.getX() * Constants.tileSize), (int) (p.getY() * Constants.tileSize));
          selectedActor.setHLColor(new Color(0, 0, 0, 0));
          selectedActor = null;
        } else if (theMap.occupant(p).equals(selectedActor)) {
          selectedActor.setHLColor(TileSheet.HLCLEAR);
          selectedActor = null;
        } else {
          System.err.println("occupied!");
        }
      }

    } else {
      if (Constants.mapMotion)
        System.err.println("mapMotion!");
      if (Constants.projectileCount > 0)
        System.err.println("projectileCount: " + Constants.projectileCount);
    }
  }

  void doAdd(Point p, Queue q) {
    if (!q.isEmpty())
      theMap.add(q.dequeue(), p, q.getLocation());
  }

  void doAction(Actor theActor) {
    if (theActor != null) {
      if (theActor.getAction() == Actor.action.HEAL) {
        theActor.setTarget(theMap.findHealTarget(theActor));
        if (theActor.getTarget() != null)
          theActor.heal(theActor.getTarget());
      }
      if (theActor.getAction() == Actor.action.ATTACK) {
        theActor.setTarget(theMap.findAttackTarget(theActor));
        if (theActor.getTarget() != null)
          theActor.attack(theActor.getTarget());
      }
      if (theActor.getTarget() != null && theActor.getTarget().getAttrib("hp") <= 0)
        theActor.setTarget(null);
    }
  }

  void cpuTurn() {
    theMap.incTurn();
    Point p = theMap.findOpen();
    doClick(p, theQueue[theMap.getTurn()]);
  }

  void paintGameImg(ImageObserver io) {
    gameG.setColor(TileSheet.BGCOLOR);
    gameG.fillRect(0, 0, (int) gameArea.getWidth(), (int) gameArea.getHeight());
    gameG.drawImage(theStatus.getImg(), theStatus.getX(), theStatus.getY(), io);
    gameG.drawImage(theMap.getImg(theTileSheet, io), theMap.getX(), theMap.getY(), io);
    for (Queue q : theQueue)
      gameG.drawImage(q.getImg(), q.getX(), q.getY(), io);
  }

  void processClick(Point p) {
    if (theMap.getTurn() < Constants.numQueues) {
      if (theMap.getBounds().contains(p)) {
        p.translate(-theMap.getX(), -theMap.getY());
        p.setLocation((int) (p.getX() / Constants.tileSize), (int) (p.getY() / Constants.tileSize));
        doClick(p, theQueue[theMap.getTurn()]);
        if (theMap.getTurn() == Constants.numQueues) {
          System.err.println("playing round");
          theMap.activate();
          theMap.incTurn();
        }
      }
    }
  }

}
