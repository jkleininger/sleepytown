package net.qrab.sleepytown;

import java.awt.image.BufferedImage;
import java.io.IOException;
import java.util.ArrayList;
import java.util.EnumMap;
import java.util.List;
import java.util.Map;

import javax.imageio.ImageIO;

public class Sprite {

  private BufferedImage[]                        sheet;
  private Map<Actor.status, List<BufferedImage>> img        = new EnumMap<Actor.status, List<BufferedImage>>(Actor.status.class);

  private Actor.status                           curState;
  private int                                    curFrame;

  private String                                 name;

  private int                                    frameDelay = 100000000;
  private long                                   lastUpdate = System.nanoTime();

  public Sprite(String fname) {
    BufferedImage inImg;
    try {
      inImg = ImageIO.read(getClass().getResource("sprites/" + fname + ".png"));
    } catch (IOException ioe) {
      return;
    }
    setName(fname);
    setState(Actor.status.IDLE);
    int cols = (int) (inImg.getWidth() / Constants.tileSize);
    int rows = (int) (inImg.getHeight() / Constants.tileSize);
    sheet = new BufferedImage[cols * rows];
    for (int r = 0; r < rows; r++) {
      for (int c = 0; c < cols; c++) {
        sheet[(r * cols) + c] = inImg.getSubimage(c * Constants.tileSize, r * Constants.tileSize, Constants.tileSize, Constants.tileSize);
      }
    }
  }

  void addState(Actor.status s) {
    img.put(s, new ArrayList<BufferedImage>());
  }

  void addFrame(Actor.status s, BufferedImage i) {
    if (img.get(s) == null)
      addState(s);
    img.get(s).add(i);
  }

  void addFrame(Actor.status s, int t) {
    addFrame(s, sheet[t]);
  }

  void addFrames(Actor.status s, int[] tiles) {
    for (int t : tiles)
      addFrame(s, t);
  }

  void addFrames(int[][] tiles) {
    for (int state = 0; state < tiles.length; state++) {
      for (int frame = 0; frame < tiles[state].length; frame++) {
        addFrame(Actor.status.values()[state], tiles[state][frame]);
      }
    }
  }

  BufferedImage getImg() {
    if (curFrame >= img.get(curState).size()) {
      curFrame = 0;
    }
    return img.get(curState).get(curFrame);
  }

  void setState(Actor.status s) {
    curState = s;
  }

  Actor.status getState() {
    return curState;
  }

  void setFrame(int fnum) {
    curFrame = fnum;
  }

  int getFrame() {
    return curFrame;
  }

  String getName() {
    return name;
  }

  void setName(String n) {
    name = n;
  }

  void incFrame() {
    curFrame = curFrame + 1 >= img.get(curState).size() ? 0 : curFrame + 1;
  }

  void update() {
    if ((System.nanoTime() - lastUpdate) > frameDelay) {
      lastUpdate = System.nanoTime();
      incFrame();
    }
  }

}
